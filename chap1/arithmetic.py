import math

# comment. this will not be executed. shortcut in vsc Ctl + /
# arithmetic in Python

# addition
tot1 = 3 + 7 # tot1 -> a variable
print(f"3 + 7 = {tot1}")
print("3 + 7 = " + str(tot1))

# substraction
tot2 = 10 - 5
print(f"10 - 5 = {tot2}")

# multiplication
# "" and '' is equal in python
tot3 = 10 * 5
print(f'10 * 5 = {tot3}')

# division
tot4 = 10 / 4
print(f"10 / 4 = {tot4}")

# division reminder 10/3. result = 3, reminder = 1
# % = modulous
tot5 = 10 % 3
print(f"the reminder of 10 / 3 = {tot5}")

# power
tot6 = 2**3 # 2 x 2 x 2
print(f"2 power by 3 = {tot6}")

tot7 = pow(2,4)
print(f"2 power by 4 = {tot7}")

# square root
tot8 = math.sqrt(64) # 8
print(f"square root of 64 = {tot8}")