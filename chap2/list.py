# index of a list start with 0
numbers = [1,2,3,4,5]
print(f'2nd postion item = {numbers[1]}')

# append(), remove() - methods
names = ['Ali', 'Jc', 'May']
names.append('John Doe')
print(f'3rd person = {names[2]}')
names.remove('Ali')
print(names) # print all data

# list can store mixed of data types, but not
# a good practice
mixed = ['Ali', 45, True]
print(f"{mixed[2]}")

