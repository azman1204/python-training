# map have { key : value }
person = {'name': 'John Doe', 'age' : 45, 'addr': 'London'}
print(f'name = {person['name']} age = {person['age']}')

# store multiple student info in a map and list
students = [
    {'name': 'stu1', 'matrix': 1}, 
    {'name': 'stu2', 'matrix': 2}, 
    {'name': 'stu3', 'matrix': 3}
]

# print info of 2nd student
print(f"name = {students[1]['name']} matrix = {students[1]['matrix']}")