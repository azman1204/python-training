# integer vs decimal
no1 = 10
no2 = 3
# integer data type
no3 = no1 / no2
print(f"{no1} / {no2} = {no3}") # 3.3xxx - decimal

# decimal data type
no4 = no1 // no2;
print(f"{no1} / {no2} = {no4}") # 3 - integer