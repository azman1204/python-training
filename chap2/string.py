# String
name = "John Doe" # name = 'Jone Doe'
print(f"Name = {name}")

# Boolean - True / False
# normally we use Boolean in control flow
isOld = False
isMarried = True
