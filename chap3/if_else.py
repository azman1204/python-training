# logical statement
late = False
if late:
    print('You are late')

if late:
    print('You are late')
else:
    print('You are on time')

# income = 15000  
income = 120000

# 1 - 9,999
if income < 10000:  
    tax_coefficient = 0.0  #1  
# 10,000 - 29,999
elif income < 30000:  
    tax_coefficient = 0.2  #2  
# 30,000 - 99,999
elif income < 100000:  
    tax_coefficient = 0.35  #3  
# 100,000 ...
else:  
    tax_coefficient = 0.45  #4

tax = income * tax_coefficient
net_income = income - tax
print(f"income = {income} tax = {tax} net income = {net_income}")