# shortcut of if .. else
order_total = 150

if order_total > 100:
    discount = 25
else:
    discount = 0

print(f"discount = {discount}")

discount = 25 if order_total > 100 else 0
print(f"discount = {discount}")