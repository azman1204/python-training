n = 1
n = n + 1 
print(f'n={n}')
n += 1 # similar to n = n + 1. it's a shortcut
# n -= 1 
# n *= 5
# n /= 3
# n //= 1
print(f'n={n}')  # n = 3

while n < 39:
    print(f"n = {n}")
    n += 1

answer = ''
while answer != 'No':
    print(f'Your answer : {answer}')
    answer = input("Choose Yes / No : ")
