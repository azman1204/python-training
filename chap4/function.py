def greet():
    print("Good morning")

# function with parameter
def greet2(name):
    print(f"Good morning {name}")

greet() # call / execute the function
greet2('John Doe') # Good morning John Doe
# the value that we sent to a function parameter is called 'argument'

# function with multiple parameters
def add(no1, no2):
    no3 = no1 + no2
    print(f"{no1} + {no2} = {no3}")

add(4,5)

# function that return a value. 98% function will return a value
def substract(no1, no2):
    no3 = no1 - no2
    return no3

result = substract(10, 3)
print(f'10 - 3 = {result}')

# function with optional parameter
def multiply(no1, no2 = 2):
    return no1 * no2

print(multiply(10,3)) # 10 x 3
print(multiply(10)) # 10 x 2