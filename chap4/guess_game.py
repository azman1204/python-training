# generate a random nom between 1 - 100
import random

def game():
    answer = random.randrange(1,100)
    player_answer = 0
    num_trials = 0
    print(answer)
    while player_answer != answer:
        # int() - convert a string to a number
        player_answer = int(input("Guess the number : "))

        if player_answer > answer:
            print("Guess Lower")
        
        if player_answer < answer:
            print("Guess higher")
        
        num_trials = num_trials + 1 # num_trails += 1

        if player_answer == answer:
            print(f"You win after {num_trials} trials")
        
game()