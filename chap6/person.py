class Person:
    # attribute / property
    # self - represent the object
    # constructor - to initialize the property value
    # run automatically when we create object out of a class
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def showInfo(self):
        return f"name = {self.name}, age = {self.age}"
    
    # decorator - to give extra info to a method
    @staticmethod
    def commonInfo():
        print("Common person info")

john = Person('Abu', 50)
print(john.showInfo())

# static method, you can call without creating the object first
Person.commonInfo()