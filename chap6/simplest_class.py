class Simplest:
    pass # pass - to indicate this class has no content

# everything in python is object
class Car:
    # class members: 1. property / attribute 2. method / function

    def __init__(self):
        # property of a car
        self.color = 'blue'
        self.brand = 'Honda'

    def info(self):
        print("Car info here ....")
    
    def changeGear(self):
        pass

    def accelerate(self):
        pass

car1 = Car() # car1 is an object. instantiate
car1.info()

car2 = Car() # car2 also an object