# open a file, and read the content. During printing, make it uppercase
path = 'C:\\Users\\azman\\Desktop\\2024\\Python Trainocate\\python_tot\\chap8\\person.txt'
file = open(path)
for name in file.readlines():
    print(f'name = {name.upper()}')

file.close()