import os
# read file
path = os.getcwd() + "\\chap8\\person.txt" # return the current path c:\...\chap8
with open(path) as file:
    for name in file.readlines():
        print(name)