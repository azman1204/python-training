#open to a file and write some content into the file
path = r'C:\Users\azman\Desktop\2024\Python Trainocate\python_tot\chap8\person.txt'
# w - overwrite all the file content
# a - append
# r - reading
file = open(path, 'a')
file.write("\nAli") # \n - print to next line
file.write("\nTerry")
file.close()