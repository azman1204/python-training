import requests
from datetime import datetime

url = 'http://jomdemy.com/images/flutter2.png'
response = requests.get(url)

if response.status_code == 200:
    # successfully download
    dt = datetime.now().strftime("%b-%d-%Y-%H-%m-%S")
    # print(dt)
    # exit()
    with open(f'{dt}-flutter.png', 'wb') as file:
        file.write(response.content)
else:
    # failed to download
    print('Something went wrong')