# read data from excel / csv and return list of Items
import os
from good import Item
import re

def item_database():
    path = os.path.dirname(os.path.abspath(__file__))
    path2 = path + "/stuff.csv"
    # print(path2)
    file = open(path2, 'r')
    items = []
    num = 0
    for item in file.readlines():
        # skip first row, because it's a header
        if num == 0:
            num += 1
            continue # continue the loop without exec the next line
        
        # remove new line char \n
        item = re.sub(r'\n', '', item)
        alist = item.split(',') # [1, 'Onion' 5.5]
        no = int(alist[0]) # convert string to int
        price = float(alist[2]) # convert string to decimal
        items.append(Item(no, alist[1], price))
    
    # for good in items:
    #     print(f'{good.no} {good.name} {good.price}')

    return items