# investment calculator

# 1. asking input: amount, duration
amount   = int(input("Amoun RM : "))
duration = int(input("Duration (years) : "))

# calculate the profit and print the result
no = 1
year = 2024
while no <= duration:
    # print(f"no = {no}")
    profit = amount * 0.06
    amount2 = f'{amount:.2f}'
    # print(f'{year:6} {amount2:12} {profit:.2f}')
    print(f'{year:10} {amount2:16} {profit:.2f}')
    amount += profit # re-investment
    no += 1 # no = no + 1
    year += 1
