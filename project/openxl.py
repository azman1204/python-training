import openpyxl as xls
from openpyxl.chart import BarChart, Reference
import os

path = os.path.dirname(__file__) + "/sales.xlsx"
wb = xls.load_workbook(path)
sheet = wb['mysales']

# manipulate data in excel file. add a new colum (price after discount)
for row in range(2, sheet.max_row + 1):
    print(row)
    new_val = sheet.cell(row,3).value
    new_cell = sheet.cell(row,4)
    new_cell.value = new_val * 0.9

# generate bar chart
values = Reference(sheet, min_row=2, max_row=sheet.max_row, min_col=4, max_col=4)
chart = BarChart()
chart.add_data(values)
sheet.add_chart(chart, 'e2')

wb.save("sales2.xlsx")