# mini project of Point of Sales
# purpose : to implement all the fundamentals of python we learnt yesterday
# MySQL, MSSQL, Oracle, Informix...

# import math
# import os
# import item
from good import Item
from inventory import item_database

# 1. database of items
# items = [
#     Item(1, 'Ketchup', 5.0),
#     Item(2, 'Sugar 1kg', 4.0),
#     Item(3, 'Egg', 7.50),
# ]

items = item_database()

# search for an item in the database
def getItem(no):
    for item in items:
        if item.no == no:
            return item

# print(getItem(3).name)
# exit() # stop here

for item in items:
    print(f'id: {item.no}, name: {item.name}, price: {item.price}')

# 2. key in purchased items
purchase = [] # list of map
while True:
    item_no = int(input("Item No : "))
    qty = int(input("Quantity : "))
    purchase.append({"item_no": item_no, "qty": qty})
    finished = input("More items [y/n] ? ")
    
    if finished == 'n':
        break # exit the loop

# for item in purchase:
#     print(f'no: {item['item_no']}, qty: {item['qty']}')

# apply tax and print the bill
# 7% x total purchase = SST
total_purchase = 0
for item in purchase:
    item_no = item['item_no']
    good = getItem(item_no)
    total_row = good.price * item['qty']
    total_purchase = total_purchase + total_row
    print(f'no: {item['item_no']}, qty: {item['qty']}, name: {good.name}, total: {total_row}')

tax = 0.07 * total_purchase
to_pay = total_purchase + tax

print(f'tax: {tax:.2f}')
print(f'TOTAL: {to_pay}')